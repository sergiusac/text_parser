package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.IPart;
import epam.javacourse.epamtextparser.ast.PartType;

import java.util.regex.Pattern;

public class SentenceParser implements IParser {

    private final IParser nextParser = new WordParser();
    private final String END_OF_SENTENCE = "[!\\.\\?]";
    private final Pattern pattern = Pattern.compile(END_OF_SENTENCE);

    @Override
    public CompoundPart parse(String text) {
        CompoundPart wordsAndSymbols = nextParser.parse(text);

        CompoundPart result = new CompoundPart();
        CompoundPart sentence = new CompoundPart(PartType.SENTENCE);

        for (IPart wordOrSymbol : wordsAndSymbols.getChildren()) {
            String childStr = wordOrSymbol.print();
            boolean matchResult = pattern.matcher(childStr).matches();
            sentence.add(wordOrSymbol);
            if (matchResult) {
                result.add(sentence);
                sentence = new CompoundPart(PartType.SENTENCE);
            }
        }
        if (sentence.getChildren().size() > 0) {
            result.add(sentence);
        }

        return result;
    }
}
