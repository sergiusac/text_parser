package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.IPart;
import epam.javacourse.epamtextparser.ast.PartType;

import java.util.regex.Pattern;

public class WordParser implements IParser {

    private final IParser nextParser = new SymbolParser();
    private final String LETTER = "[a-zA-Z]";
    private final Pattern pattern = Pattern.compile(LETTER);

    @Override
    public CompoundPart parse(String text) {
        CompoundPart symbols = nextParser.parse(text);

        CompoundPart result = new CompoundPart();
        CompoundPart word = new CompoundPart(PartType.WORD);

        for (IPart symbol : symbols.getChildren()) {
            boolean matchResult = pattern.matcher(symbol.print()).matches();
            if (matchResult) {
                word.add(symbol);
            } else {
                if (word.getChildren().size() > 0) {
                    result.add(word);
                    word = new CompoundPart(PartType.WORD);
                }
                result.add(symbol);
            }
        }

        return result;
    }
}
