package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.PartType;

public class TextParser implements IParser {

    private final IParser nextParser = new ParagraphParser();

    @Override
    public CompoundPart parse(String text) {
        CompoundPart parsedText = nextParser.parse(text);
        parsedText.setType(PartType.TEXT);
        return parsedText;
    }
}
