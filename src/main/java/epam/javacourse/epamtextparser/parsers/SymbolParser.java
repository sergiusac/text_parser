package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.LeafPart;

public class SymbolParser implements IParser {

    @Override
    public CompoundPart parse(String text) {
        CompoundPart result = new CompoundPart();

        for (int i = 0; i < text.length(); i++) {
            result.add(new LeafPart(text.charAt(i)));
        }

        return result;
    }
}
