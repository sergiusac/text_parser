package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;

public interface IParser {
    CompoundPart parse(String text);
}
