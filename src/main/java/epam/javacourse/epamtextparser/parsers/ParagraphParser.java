package epam.javacourse.epamtextparser.parsers;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.IPart;
import epam.javacourse.epamtextparser.ast.PartType;

import java.util.regex.Pattern;

public class ParagraphParser implements IParser {

    private final IParser nextParser = new SentenceParser();
    private final String END_OF_PARAGRAPH = "\n\\s+";
    private final Pattern pattern = Pattern.compile(END_OF_PARAGRAPH);

    @Override
    public CompoundPart parse(String text) {
        CompoundPart sentences = nextParser.parse(text);

        CompoundPart result = new CompoundPart();
        CompoundPart paragraph = new CompoundPart(PartType.PARAGRAPH);

        for (IPart sentence : sentences.getChildren()) {
            String childStr = sentence.print();
            boolean matchResult = pattern.matcher(childStr).find();
            if (matchResult) {
                result.add(paragraph);
                paragraph = new CompoundPart(PartType.PARAGRAPH);
            }
            paragraph.add(sentence);
        }
        if (paragraph.getChildren().size() > 0) {
            result.add(paragraph);
        }

        return result;
    }
}
