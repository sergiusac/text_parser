package epam.javacourse.epamtextparser;

import epam.javacourse.epamtextparser.ast.*;
import epam.javacourse.epamtextparser.comparators.PartComparator;
import epam.javacourse.epamtextparser.parsers.*;
import epam.javacourse.epamtextparser.sorting.Sorter;

import java.io.*;
import java.util.List;

public class Main {

    private static final String filePath = "data/input.txt";

    public static void main(String[] args) {
        String inputText = readText(filePath);

        CompoundPart text= new TextParser().parse(inputText);

        System.out.println("-----Parsed text-----\n" + text.print());

        System.out.println("\n-----Paragraphs sorted by the number of sentences-----");
        PartComparator comparator = new PartComparator(PartType.PARAGRAPH, PartType.SENTENCE);
        List<IPart> sortedParagraphs = Sorter.sort(text, comparator);
        for (int i = 0; i < sortedParagraphs.size(); i++) {
            System.out.printf("Paragraph %d: %s%n", i, sortedParagraphs.get(i).print());
        }

        System.out.println("\n-----Sentences of paragraph 2 sorted by the number of words-----");
        comparator.setTypeForSorting(PartType.SENTENCE);
        comparator.setTypeToSortBy(PartType.WORD);
        List<IPart> sortedSentences = Sorter.sort((CompoundPart) sortedParagraphs.get(2), comparator);
        for (int i = 0; i < sortedSentences.size(); i++) {
            System.out.printf("Sentence %d: %s%n", i, sortedSentences.get(i).print());
        }

        System.out.println("\n-----Words of sentence 0 sorted by the number of symbols-----");
        comparator.setTypeForSorting(PartType.WORD);
        comparator.setTypeToSortBy(PartType.SYMBOL);
        List<IPart> sortedWords = Sorter.sort((CompoundPart) sortedSentences.get(0), comparator);
        for (int i = 0; i < sortedWords.size(); i++) {
            System.out.printf("Word %d: %s%n", i, sortedWords.get(i).print());
        }
    }

    private static String readText(String filePath) {
        StringBuilder builder = new StringBuilder();
        try {
            Reader reader = new FileReader(new File(filePath));
            int c = 0;
            while ((c = reader.read()) != -1) {
                builder.append((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
