package epam.javacourse.epamtextparser.ast;

public enum PartType {
    SYMBOL, WORD, SENTENCE, PARAGRAPH, TEXT
}
