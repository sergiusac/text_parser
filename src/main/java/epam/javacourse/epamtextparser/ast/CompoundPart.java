package epam.javacourse.epamtextparser.ast;

import java.util.ArrayList;
import java.util.List;

public class CompoundPart implements IPart {

    private List<IPart> children;
    private PartType type;

    public CompoundPart() {
        this(new ArrayList<>(), null);
    }

    public CompoundPart(List<IPart> children) {
        this(children, null);
    }

    public CompoundPart(PartType type) {
        this(new ArrayList<>(), type);
    }

    public CompoundPart(List<IPart> children, PartType type) {
        this.children = children;
        this.type = type;
    }

    public void add(IPart child) {
        children.add(child);
    }

    public boolean remove(IPart child) {
        return children.remove(child);
    }

    public void setType(PartType type) {
        this.type = type;
    }

    public List<IPart> getChildren() {
        return children;
    }

    @Override
    public String print() {
        return children.stream().map(IPart::print).reduce(String::concat).orElse("");
    }

    @Override
    public PartType getType() {
        return type;
    }
}
