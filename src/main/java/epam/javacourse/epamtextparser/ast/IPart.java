package epam.javacourse.epamtextparser.ast;

public interface IPart {
    String print();
    PartType getType();
}
