package epam.javacourse.epamtextparser.ast;

public class LeafPart implements IPart {

    private char symbol;
    private final PartType type = PartType.SYMBOL;

    public LeafPart() {
    }

    public LeafPart(char symbol) {
        this.symbol = symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    @Override
    public String print() {
        return String.valueOf(symbol);
    }

    @Override
    public PartType getType() {
        return type;
    }
}
