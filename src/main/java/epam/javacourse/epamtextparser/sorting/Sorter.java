package epam.javacourse.epamtextparser.sorting;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.IPart;
import epam.javacourse.epamtextparser.comparators.PartComparator;

import java.util.List;
import java.util.stream.Collectors;

public class Sorter {

    public static List<IPart> sort(CompoundPart compoundPart, PartComparator comparator) {
        return compoundPart.getChildren().stream()
                .filter(part -> part.getType().equals(comparator.getTypeForSorting()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
