package epam.javacourse.epamtextparser.comparators;

import epam.javacourse.epamtextparser.ast.CompoundPart;
import epam.javacourse.epamtextparser.ast.IPart;
import epam.javacourse.epamtextparser.ast.PartType;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PartComparator implements Comparator<IPart> {

    private PartType typeForSorting;
    private PartType typeToSortBy;

    public PartComparator(PartType typeForSorting, PartType typeToSortBy) {
        this.typeForSorting = typeForSorting;
        this.typeToSortBy = typeToSortBy;
    }

    public PartType getTypeForSorting() {
        return typeForSorting;
    }

    public void setTypeForSorting(PartType typeForSorting) {
        this.typeForSorting = typeForSorting;
    }

    public PartType getTypeToSortBy() {
        return typeToSortBy;
    }

    public void setTypeToSortBy(PartType typeToSortBy) {
        this.typeToSortBy = typeToSortBy;
    }

    @Override
    public int compare(IPart p1, IPart p2) {
        if (!p1.getType().equals(typeForSorting) || !p2.getType().equals(typeForSorting)) {
            throw new IllegalArgumentException("PartType of CompoundPart must be " + typeForSorting.toString());
        }

        List<IPart> sentences1 = ((CompoundPart) p1).getChildren().stream()
                .filter(part -> part.getType().equals(typeToSortBy))
                .collect(Collectors.toList());

        List<IPart> sentences2 = ((CompoundPart) p2).getChildren().stream()
                .filter(part -> part.getType().equals(typeToSortBy))
                .collect(Collectors.toList());

        return Integer.compare(sentences1.size(), sentences2.size());
    }
}
